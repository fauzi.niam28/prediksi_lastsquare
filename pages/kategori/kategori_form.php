<?php
  include_once("koneksi.php");
  $database = new Connection(); 
  $db = $database->openConnection();

if(!empty($_GET['id_kategori'])){
  $sql="SELECT * FROM `tb_kategori` WHERE id_kategori='$_GET[id_kategori]'";
  $data=$db->query($sql)->fetch(PDO::FETCH_ASSOC);
  $action="?pg=kategori_edit";
}else{
  $action="?pg=kategori_tambah";
}
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Form tambah/edit Kategori</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div align="center" class="panel"><a class="btn btn-info" href="?pg=admin"><i class="fa fa-chevron-left fa-fw"></i> Kembali</a></div>
        <form class="form" action="<?=$action?>" method="post">
        <table align="center" class="table">
          <tr>
            <td>Kategori</td>
            <td><input type="text" name="kategori" class="form-control" value="<?=$data[kategori]?>" /></td>
          </tr>
          <tr>
          <td>
        <?php if($_GET['act']=="edit"){ ?>
        <input type="hidden" name="id_kategori" value="<?=$data[id_kategori]?>" />
        <?php } ?>
        </td><td><button type="submit" class="btn btn-info"><i class="fa fa-save fa-fw"></i> Simpan</button> &nbsp;<button type="reset" class="btn btn-danger"><i class="fa fa-repeat fa-fw"></i> Reset</button> </td></tr>
        </table>
        </form>
    </div>
</div>