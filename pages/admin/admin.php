<?php
include_once("koneksi.php");
$database = new Connection(); 
$db = $database->openConnection();
$sql = "SELECT * FROM `tb_admin`" ;
$dat = $db->query($sql);
// print_r($dat);die();
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Daftar Admin</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <div align="center" ><a class="btn btn-info" href="index.php?pg=admin_form"><i class="fa fa-plus fa-fw"></i> Tambah</a></div>
            </div>
            <div class="panel-body">
                <table class="table">
                  <tr>
                     <th>No</th>
                    <th>Nama admin</th>
                    <th>Username</th>
                    <th>Password</th>
                     <th>Aksi</th>
                  </tr>
                <?php
                while ($data=$dat->fetch(PDO::FETCH_ASSOC)){ 
                $i++;?>
                  <tr>
                     <td><?=$i?></td>
                    <td><?=$data["nama_lengkap"]?></td>
                    <td><?=$data["username"]?></td>
                    <td><?=$data["password"]?></td>
                     <td><a class="btn btn-info" href="?pg=admin_form&act=edit&id_admin=<?=$data["id_admin"]?>"><i class="fa fa-pencil fa-fw"></i> Edit</a>&nbsp;
                <a class="btn btn-danger" href="?pg=admin_hapus&id_admin=<?=$data["id_admin"]?>"><i class="fa fa-trash-o fa-fw"></i>Hapus</a></td>
                  </tr>
                <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>