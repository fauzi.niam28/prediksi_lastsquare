<?php
include_once("koneksi.php");
include_once 'Perhitungan/Leastsquare.php';
    $least= new \Perhitungan\Leastsquare;
    $database = new \Connection; 
    $db = $database->openConnection();
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Prediksi</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">   
        <div class="panel panel-default">
            <!-- <div class="panel-heading">
                <form action="?pg=apriori_proses" method="post">
                    <div class="form-inline">
                      <div class="form-group">
                        <label for="exampleInputName2">Min. Support</label>
                        <input type="text" class="form-control" name="min_support" placeholder="50">
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputName2">Min. Confiden</label>
                        <input type="text" class="form-control" name="min_confiden" placeholder="70">
                      </div>
                      <button type="submit" class="btn btn-primary">lihat</button>  
                    </div>
                </form>
            </div> -->
            <div class="panel-body">
                <?php 
                    $sql = "SELECT  *, SUM(`jumlah`) AS jml, YEAR(tgl_transaksi) AS tahun, MONTH(tgl_transaksi) bulan FROM `tb_transaksi` `a`  LEFT JOIN `tb_transaksi_detail` `b`  ON b.id_transaksi = a.`id_transaksi`  GROUP BY id_menu, YEAR(`tgl_transaksi`) order by `id_menu` asc, `tahun` asc";
                    
                    $data_res= $db->query($sql)->fetchAll();
                    print_r($data_res);

                    $new_array=array();

                    $no=0;
                    $last_idmenu='';
                    foreach ($data_res as $row) {
                        echo " ID: ".$row['id_menu'] . "<br>";           
                        echo " Name: ".$row['tahun'] . "<br>";
                        if($last_idmenu==''){
                            $last_idmenu=$row['id_menu'];
                        }

                        if($last_idmenu==$row['id_menu']){
                            $new_array[$no]['id_menu']=$row['id_menu'];
                            $new_array[$no]['data'][]=array(
                                'id_menu'=>$row['id_menu'],
                                'tahun'=>$row['tahun'],
                                'penjualan'=>$row['jml']
                            );
                        }

                        if($last_idmenu!=$row['id_menu']){
                            $last_idmenu=$row['id_menu'];
                            $no=$no+1;

                            $new_array[$no]['id_menu']=$row['id_menu'];
                            $new_array[$no]['data'][]=array(
                                'id_menu'=>$row['id_menu'],
                                'tahun'=>$row['tahun'],
                                'penjualan'=>$row['jml']
                            );
                        }
                    }

                    print_r($new_array);

                    foreach ($new_array as $value) {
                        
                    }

                    $data=[
                        ['tahun'=> '1996', 'penjualan'=> '15000'],
                        ['tahun'=> '1997', 'penjualan'=> '16000'],
                        ['tahun'=> '1998', 'penjualan'=> '17000'],
                        ['tahun'=> '1999', 'penjualan'=> '17500'],
                        ['tahun'=> '2000', 'penjualan'=> '18000'],
                        ['tahun'=> '2001', 'penjualan'=> '18500'],
                        ['tahun'=> '2002', 'penjualan'=> '18500'],
                        ['tahun'=> '2003', 'penjualan'=> '19000'],
                        ['tahun'=> '2004', 'penjualan'=> '19250'],
                        ['tahun'=> '2005', 'penjualan'=> '19500'],
                        ['tahun'=> '2006', 'penjualan'=> '19750']
                    ];

                    $data_2=[
                        ['tahun'=> '1997', 'penjualan'=> '16250'],
                        ['tahun'=> '1998', 'penjualan'=> '17200'],
                        ['tahun'=> '1999', 'penjualan'=> '18050'],
                        ['tahun'=> '2000', 'penjualan'=> '18800'],
                        ['tahun'=> '2001', 'penjualan'=> '19450'],
                        ['tahun'=> '2002', 'penjualan'=> '20000'],
                        ['tahun'=> '2003', 'penjualan'=> '20450'],
                        ['tahun'=> '2004', 'penjualan'=> '20800'],
                        ['tahun'=> '2005', 'penjualan'=> '21050'],
                        ['tahun'=> '2006', 'penjualan'=> '21250']
                    ];

                    $jml_data=count($data_2);
                    $data_analisis=$least->analisis($data_2,$jml_data);

                    $jml_penjualan_y=$least->sum_penjualan_y($data_analisis);
                    $jml_prediksi_x=$least->sum_prediksi_x($data_analisis);
                    $jml_x_2=$least->sum_x_2($data_analisis);
                    $jml_x_y=$least->sum_x_y($data_analisis);

                    $nilai_a=$least->nilai_a($jml_penjualan_y, $jml_data);
                    $nilai_b=$least->nilai_b($jml_x_y, $jml_x_2);

                    $sesudah=$least->sesudah($data_analisis, $nilai_a, $nilai_b, 1);// 3 tahun sesudah
                    $sebelum=$least->sebelum($data_analisis, $nilai_a, $nilai_b, 1);// 2 tahun sebelum

                    print_r($data) ;
                    // echo $nilai_a.'-'.$nilai_b;
                    echo ' <br> '.$sesudah;
                    echo ' <br> '.$sebelum;
                ?>
            </div>
        </div>
    </div>
</div>