<?php
date_default_timezone_set('Asia/Jakarta');
include_once("koneksi.php");
include_once 'Perhitungan/Leastsquare.php';
    $least= new \Perhitungan\Leastsquare;
    $database = new \Connection; 
    $db = $database->openConnection();

    $thisday=date('Y-m-d');
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Prediksi</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12"> 
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="index.php?pg=prediksi" method="POST" accept-charset="utf-8" >
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2">Produk</label>
                        <div class="col-sm-4">
                            <select class="form-control select2-single" name="produk" id="produk">
                                <option></option>
                                <?php 
                                    $sql2="select * from `tb_menu` join `tb_kategori` on `tb_kategori`.`id_kategori`=`tb_menu`.`id_kategori`";
                                    $produk = $db->query($sql2);
                                    $label='';

                                    while ($p=$produk->fetch(PDO::FETCH_ASSOC)) {
                                        if($label==''){
                                            $label=$p['kategori'];
                                            echo '<optgroup label="'.$p[kategori].'">';
                                        }
                                        if($label==$p[kategori]){
                                            echo '<option value="'.$p[id_menu].'-'.$p[nama_menu].'">'.$p[nama_menu].'</option>';
                                        }
                                        if($label != $p[kategori]){
                                            echo '</optgroup>';
                                            echo '<optgroup label="'.$p[kategori].'">';
                                        }

                                    }
                                ?>                       

                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Prediksi</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <?php if (!empty($_POST)): 
            $explod=explode('-', $_POST['produk']);
            $id=$explod[0];
            $nama=$explod[1];
        ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Analisa Data <strong><?=$nama ?></strong>
                </div>
                <div class="panel-body">
                    <?php 
                        $sql = "SELECT 
                                  *,
                                  SUM(`jumlah`) AS jml,
                                  YEAR(tgl_transaksi) AS tahun,
                                  MONTH(tgl_transaksi) bulan 
                                FROM
                                  `tb_transaksi` `a` 
                                  LEFT JOIN `tb_transaksi_detail` `b` 
                                    ON b.id_transaksi = a.`id_transaksi` 
                                  LEFT JOIN `tb_menu` `c` 
                                    ON c.id_menu = b.id_menu 
                                    WHERE b.id_menu='".$id."'
                                GROUP BY 
                                  YEAR(`tgl_transaksi`), MONTH(`tgl_transaksi`) 
                                ORDER BY 
                                  `tahun` DESC, `bulan` DESC
                                 LIMIT 12";
                        
                        $data_res= $db->query($sql)->fetchAll();
                        // print_r($data_res);

                        $new_array=array();

                        $no=0;
                        $last_idmenu='';
                        foreach ($data_res as $row) {
                            $tahun=$row['tahun'];
                            $bulan=$row['bulan'];
                            $new_array[$tahun][$bulan]['bulan']=$row['bulan'];
                            $new_array[$tahun][$bulan]['tahun']=$row['tahun'];
                            $new_array[$tahun][$bulan]['data']=array(
                                // 'bulan'=>$row['bulan'],
                                'tahun'=>$row['bulan'],
                                'penjualan'=>$row['jml']
                            );
                        }

                        $arr_bulan=array();
                        for ($i = 11; $i >= 0; $i--) {
                            $tgl = date('Y-m-d', strtotime('-'.$i.' month', strtotime($thisday))); //operasi penjumlahan 
                            $arrdate=explode('-', $tgl);
                            $arr_bulan[]=array(
                                'tahun'=>intval($arrdate[0]),
                                'bulan'=>intval($arrdate[1])
                            );
                        }

                        $arr_data=array();
                        foreach ($arr_bulan as $val) {
                            $tahun=$val['tahun'];
                            $bulan=$val['bulan'];
                            $get_data=$new_array[$tahun][$bulan]['data'];
                            if (!empty($get_data)) {
                                $arr_data[]=array(
                                    'tahun'=>$tahun,
                                    'bulan'=>$bulan,
                                    'penjualan'=>$get_data['penjualan']
                                );
                            } else {
                                $arr_data[]=array(
                                    'tahun'=>$tahun,
                                    'bulan'=>$bulan,
                                    'penjualan'=>0
                                );
                            }
                        }
                  
                        $jml_data=count($arr_data);
                        $data_analisis=$least->analisis($arr_data,$jml_data);

                        // print_r($data_analisis);die();

                        $jml_penjualan_y=$least->sum_penjualan_y($data_analisis);
                        $jml_prediksi_x=$least->sum_prediksi_x($data_analisis);
                        
                        $jml_x_2=$least->sum_x_2($data_analisis);
                        $jml_x_y=$least->sum_x_y($data_analisis);

                        $nilai_a=$least->nilai_a($jml_penjualan_y, $jml_data);
                        $nilai_b=$least->nilai_b($jml_x_y, $jml_x_2);

                        $sesudah=$least->sesudah($data_analisis, $nilai_a, $nilai_b, 1);
                        
                    ?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun</th>
                                <th>Bulan</th>
                                <th>Penjualan (y)</th>
                                <th>Prediksi (x)</th>
                                <th>X2</th>
                                <th>XY</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no_dat=1;
                                foreach ($data_analisis as $row) {
                                    echo '
                                        <tr>
                                            <td>'.$no_dat.'</td>
                                            <td>'.$row['tahun'].'</td>
                                            <td>'.$row['bulan'].'</td>
                                            <td>'.$row['penjualan_y'].'</td>
                                            <td>'.$row['prediksi_x'].'</td>
                                            <td>'.$row['x_2'].'</td>
                                            <td>'.$row['x_y'].'</td>
                                        </tr>
                                    ';
                                    $no_dat++;
                                }
                            ?>
                        </tbody>
                    </table>

                    <div class="alert alert-success" role="alert">Prediksi Stok Bulan Depan : <strong><?=$sesudah ?></strong></div>
                </div>
            </div>     
        <?php endif ?> 
        
    </div>
</div>