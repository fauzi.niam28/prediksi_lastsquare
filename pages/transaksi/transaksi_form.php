<?php
include_once("koneksi.php");
$database = new Connection(); 
$db = $database->openConnection();
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Transaksi</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">
        <form action="?pg=transaksi_tambah" method="post">    
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php 
                        // $carikode = mysqli_query($conn,"select max(no_faktur) from tb_transaksi") or die (mysqli_error());
                        // menjadikannya array
                        // $datakode = mysqli_fetch_array($carikode);
                        $sql ="select max(no_faktur) as max from tb_transaksi";
                        $datakode = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
                        // jika $datakode
                        if ($datakode) {
                           $nilaikode = substr($datakode['max'], 1);
                           // menjadikan $nilaikode ( int )
                           $kode = (int) $nilaikode;
                           // setiap $kode di tambah 1
                           $kode = $kode + 1;
                           $kode_otomatis ="1".str_pad($kode, 4, "0", STR_PAD_LEFT);
                        } else {
                            $kode_otomatis = "0001";
                        }
                    ?>
                    <div class="form-inline">
                      <div class="form-group">
                        <label for="exampleInputName2">No Faktur </label>
                        <input type="text" class="form-control" name="no_faktur" value="<?php echo $kode_otomatis; ?>" readonly>
                      </div>   
                    </div>
                </div>
                <div class="panel-body">
                    
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Produk</label>
                        <div class="col-sm-4">
                            <select class="form-control select2-single" name="produk" id="produk">
                                <option></option>
                                <?php 
                                    $sql2="select * from `tb_menu` join `tb_kategori` on `tb_kategori`.`id_kategori`=`tb_menu`.`id_kategori`";
                                    $produk = $db->query($sql2);
                                    $label='';

                                    while ($p=$produk->fetch(PDO::FETCH_ASSOC)) {
                                        if($label==''){
                                            $label=$p['kategori'];
                                            echo '<optgroup label="'.$p[kategori].'">';
                                        }
                                        if($label==$p[kategori]){
                                            echo '<option value="'.$p[id_menu].'">'.$p[nama_menu].'</option>';
                                        }
                                        if($label != $p[kategori]){
                                            echo '</optgroup>';
                                            echo '<optgroup label="'.$p[kategori].'">';
                                        }

                                    }
                                ?>                       

                            </select>
                        </div>
                        <div class="col-md-1">
                            <input type="number" name="qty" id="qty" class="form-control" value="1" min="1">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-default" onclick="addcart();">Add Cart</button>
                        </div>
                    </div>
                    <hr>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>kategori</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Act</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no=1;
                                $sql3="select * from `tb_cart` join `tb_menu` on `tb_menu`.`id_menu`=`tb_cart`.`id_menu` join `tb_kategori` on `tb_kategori`.`id_kategori`=`tb_menu`.`id_kategori`";
                                $cart = $db->query($sql3);
                                while ($c=$cart->fetch(PDO::FETCH_ASSOC)) {
                                    echo "
                                        <tr>
                                            <td>".$no."</td>
                                            <td>".$c['kategori']."</td>
                                            <td>".$c['nama_menu']."</td>
                                            <td>".$c['jml']."</td>
                                            <td><a class=\"btn btn-danger\" href=\"?pg=transaksi_hapuscart&id_cart=".$c['id_cart']."\"><i class=\"fa fa-trash-o fa-fw\"></i>Hapus</a></td>
                                        </tr>
                                    ";
                                    $no++;
                                }
                            ?>
                            
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <!-- <button type="reset" class="btn btn-warning">Ulangi</button> -->
                </div>
            </div>
        </form>
    </div>
</div>

