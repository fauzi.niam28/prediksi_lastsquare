<?php
include_once("koneksi.php");
$database = new Connection(); 
$db = $database->openConnection();
$sql="SELECT  `tb_transaksi`.`id_transaksi`, `no_faktur`, tgl_transaksi, jumlah, harga, SUM(jumlah * harga) AS total 
  FROM `tb_transaksi` 
  INNER JOIN `tb_transaksi_detail` ON `tb_transaksi_detail`.`id_transaksi`=`tb_transaksi`.`id_transaksi`
  LEFT JOIN `tb_menu` ON `tb_menu`.`id_menu` = `tb_transaksi_detail`.`id_menu` 
  GROUP BY `no_faktur` ";
$dat = $db->query($sql);

?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Daftar Transaksi</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <div align="center" ><a class="btn btn-info" href="?pg=transaksi_form"><i class="fa fa-plus fa-fw"></i> Tambah</a></div>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>No Faktur</th>
                        <th>Tgl Transaksi</th>
                        <th>Total Bayar</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($data=$dat->fetch(PDO::FETCH_ASSOC)){ 
                        $i++;
                        ?>
                          <tr>
                            <td><?=$i?></td>
                            <td><?=$data["no_faktur"]?></td>
                            <td><?=$data["tgl_transaksi"]?></td>
                            <td><?=$data["total"]?></td>
                            <td><a class="btn btn-info" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" id="detail" data-data="transaksi_detail.php" data-id="<?=$data["id_transaksi"]?>"><i class="fa fa-info fa-fw"></i> Detail</a>&nbsp;
                        <a class="btn btn-danger" href="?pg=transaksi_hapus&id_transaksi=<?=$data["id_transaksi"]?>"><i class="fa fa-trash-o fa-fw"></i>Hapus</a></td>
                          </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Detail Pesanan</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody id="data-det">
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
