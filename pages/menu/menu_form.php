<?php
  include_once("koneksi.php");
  $database = new Connection(); 
  $db = $database->openConnection();

if(!empty($_GET['id_menu'])){
  $sql="SELECT * FROM `tb_menu` WHERE id_menu='$_GET[id_menu]'";
  $data=$db->query($sql)->fetch(PDO::FETCH_ASSOC);
  $action="?pg=menu_edit";
}else{
  $action="?pg=menu_tambah";
}
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Form tambah/edit Menu</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div align="center" class="panel"><a class="btn btn-info" href="?pg=menu"><i class="fa fa-chevron-left fa-fw"></i> Kembali</a></div>
        <form class="form" action="<?=$action?>" method="post">
        <table align="center" class="table">
          <tr>
            <td>Kategori Menu</td>
            <td>
                <select name="id_kategori" class="form-control">
                <option>--Pilih--</option>
                <?php
                $sql_2="SELECT * FROM `tb_kategori`";
                $kategori =$db->query($sql_2);
                while ($kat=$kategori->fetch(PDO::FETCH_ASSOC)) {
                    if ($data['id_kategori']==$kat['id_kategori']) {
                        echo "<option value=\"$kat[id_kategori]\" selected>$kat[kategori]</option>";
                    }else {
                        echo "<option value=\"$kat[id_kategori]\">$kat[kategori]</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
          <tr>
            <td>Nama Menu</td>
            <td><input type="text" name="nama_menu" class="form-control" value="<?=$data[nama_menu]?>" /></td>
          </tr>
          <tr>
            <td>Harga</td>
            <td><input type="number" name="harga" class="form-control" value="<?=$data[harga]?>" /></td>
          </tr>
          <tr>
          <td>
        <?php if($_GET['act']=="edit"){ ?>
        <input type="hidden" name="id_menu" value="<?=$data[id_menu]?>" />
        <?php } ?>
        </td><td><button type="submit" class="btn btn-info"><i class="fa fa-save fa-fw"></i> Simpan</button> &nbsp;<button type="reset" class="btn btn-danger"><i class="fa fa-repeat fa-fw"></i> Reset</button> </td></tr>
        </table>
        </form>
    </div>
</div>