<?php
include_once("koneksi.php");
$database = new Connection(); 
$db = $database->openConnection();
$sql="SELECT * FROM `tb_menu` INNER JOIN `tb_kategori` ON `tb_kategori`.`id_kategori`=`tb_menu`.`id_kategori`";
$dat = $db->query($sql);
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Daftar Menu</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <div align="center" ><a class="btn btn-info" href="?pg=menu_form"><i class="fa fa-plus fa-fw"></i> Tambah</a></div>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kategori Menu</th>
                            <th>Nama Menu</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($data=$dat->fetch(PDO::FETCH_ASSOC)){ 
                        $i++;?>
                          <tr>
                            <td><?=$i?></td>
                            <td><?=$data["kategori"]?></td>
                            <td><?=$data["nama_menu"]?></td>
                            <td><?=$data["harga"]?></td>
                            <td><a class="btn btn-info" href="?pg=menu_form&act=edit&id_menu=<?=$data["id_menu"]?>"><i class="fa fa-pencil fa-fw"></i> Edit</a>&nbsp;
                        <a class="btn btn-danger" href="?pg=menu_hapus&id_menu=<?=$data["id_menu"]?>"><i class="fa fa-trash-o fa-fw"></i>Hapus</a></td>
                          </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>