<?php 
error_reporting(0);
session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Prediksi</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="vendor/select2/css/select2.min.css" rel="stylesheet">
    <link href="vendor/select2/css/select2-bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->


</head>

<body>

    <?php if (empty($_SESSION['userid'])) {
        include 'login_form.php';
    } else {?>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Prediksi</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <?php echo "$_SESSION[usernm]" ?>&nbsp;<i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.php?pg=admin"><i class="fa fa-user fa-fw"></i> Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="logoff.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <!-- <li>
                                <a href="index.php?pg=beranda"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li> -->
                             <li>
                                <a href="index.php?pg=prediksi"><i class="fa fa-table fa-fw"></i> Prediksi</a>
                            </li>
                            <li>
                                <a href="index.php?pg=admin"><i class="fa fa-user fa-fw"></i> Admin</a>
                            </li>
                            <li>
                                <a href="index.php?pg=kategori"><i class="fa fa-sitemap  fa-fw"></i> Kategori Menu</a>
                            </li>
                            <li>
                                <a href="index.php?pg=menu"><i class="fa fa-list fa-fw"></i> Daftar Menu</a>
                            </li>
                             <li>
                                <a href="index.php?pg=transaksi"><i class="glyphicon glyphicon-transfer"></i> Transaksi</a>
                            </li>
                            <!-- <li>
                                <a href="index.php?pg=laporan"><i class="fa fa-file-o fa-fw"></i> Laporan</a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
               
                <?php
                if(empty($_GET['pg']) OR $_GET['pg']=='beranda')include 'beranda.php';

                $link = $_GET['pg'];
                $link = explode("_", $link);
                $folder= $link[0];
                
                include 'pages/'.$folder.'/'.$_GET['pg'].'.php';
                ?>

            </div>
            <!-- /#page-wrapper -->
        </div>
    <!-- /#wrapper -->
    <?php } ?>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/select2/js/select2.full.js"></script>
    <script src="vendor/select2/js/anchor.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            anchors.options.placement = 'left';
            anchors.add('.container h1, .container h2, .container h3, .container h4, .container h5');
            

            $( ".select2-single, .select2-multiple" ).select2( {
                theme: "bootstrap",
                placeholder: "Select a State",
            } );

            $('#dataTables-example').DataTable({
                responsive: true
            });

            $(document).on('click', '#detail', function(event) {
                event.preventDefault();
                var faktur = $(this).data('id');
                var url = $(this).data('data');
                // alert(faktur+'_'+url);
                var req = $.ajax({
                    url: url,
                    type: 'POST',
                    //dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                    data: {id: faktur},
                })
                req.done(function(html) {
                    $('#data-det').html(html);
                })
                req.fail(function(xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                })
                
            });
        });
    </script>

    <script type="text/javascript">
        $('input[type="checkbox"]').click(function(){
            var val=$(this).data('data');

            if ($(this).is(":checked")) {
                $('.'+val).removeAttr('disabled');
                $('.'+val).val(1);
            } else {
                $('#jml.'+val).attr('disabled', 'disabled');
                $('.'+val).val("");
            }
        });

        function addcart() {
            var produk = $("#produk").val();
            var qty = $("#qty").val();
            if (produk == '') {
                return $("#produk").focus();
            }

            if (qty == '') {
                return $("#qty").focus();
            }

            $.ajax({
                url: 'cart.php',
                type: 'POST',
                dataType: 'json',
                data: {produk: produk, qty:qty},
            })
            .done(function(json) {
                if (json=='oke') {
                    location.reload();
                };
            })
            .fail(function() {
                console.log("error");
            })
        }
    </script>
</body>

</html>
