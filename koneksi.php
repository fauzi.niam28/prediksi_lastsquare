<?php
// error_reporting(0);
// $conn = mysqli_connect('localhost:3307','root','');
// mysqli_select_db($conn,"prediksi");


Class Connection {
 
	private $databaseHost = 'localhost:3307';
	private $databaseName = 'prediksi';
	private $databaseUsername = 'root';
	private $databasePassword = '';
	 
	private $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,);
	 
	protected $con;
 
  	public function openConnection(){

        try {

		    $this->con = new PDO("mysql:host={$this->databaseHost};dbname={$this->databaseName}", $this->databaseUsername, $this->databasePassword);
		    $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $this->con->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		    return $this->con;

	    }

	    catch (PDOException $e){

            echo "koneksi error: " . $e->getMessage();

        }

   	}
 
	public function closeConnection() {
	 
	   	$this->con = null;
 
	}
 
}